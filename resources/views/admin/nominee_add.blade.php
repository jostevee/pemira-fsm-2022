@include('header')
@php
  // $status = $data['status'];
  /*
  if(isset($data)){
    print($data['status']);
    foreach($data as $p){
      print($p);
    }
  }
  */
@endphp

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!-- <h1 class="logo me-auto"><a href="index.html">Medilab</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="#" class="logo me-auto"><img src="{{ asset ('assets/img/logo_optimus_navbar.webp') }}" alt="" class="img-fluid"><!-- <h4>Optimus</h4> --></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link" href="/">Home</a></li>
          <!-- <li><a class="nav-link scrollto" href="#procedure">Add Voter</a></li> -->
          <li class="dropdown"><a class="nav-link"><span>Student (Voter)</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a class="nav-link" href="/admin/student/list">List Student</a></li>
              <li><a class="nav-link" href="/admin/student/add">Add Student</a></li>
            </ul>
          </li>
          <li class="dropdown"><a class="nav-link active"><span>Nominee</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a class="nav-link" href="/admin/nominee/list">List Nominee</a></li>
              <li><a class="nav-link" href="#">Add Nominee</a></li>
            </ul>
          </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      <a href="/logout" class="appointment-btn scrollto">Logout <i class="bi bi-chevron-right"></i></a>
    </div>

  </header><!-- End Header -->


  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="col-12 acrylic justify-content-center">
      <div class="container">
        <h1>Add Nominee</h1>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us mt-4">
      <div class="container">

        <div class="row">
          <div class="col-lg-8 col-md-8 align-items-stretch">
            <div class="content">
              <!-- <div class="card-body"> -->
                <form method="POST" enctype="multipart/form-data">
                  @csrf
                  @if (session('error'))
                    <div class="alert alert-danger">
                      {{ session('error') }}
                    </div>
                  @endif
                  @if (session('success'))
                    <div class="alert alert-success">
                      {{ session('success') }}
                    </div>
                  @endif
                  <!-- <a href="/dashboard" class="mb-3 btn btn-primary">Dashboard</a> -->
                  <div class="col-12">
                    <div class="row">
                      <div class="col-3">
                        <label for="nama" class="col-6">Name</label>
                      </div>
                      <div class="col-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter nominee's name..." required>
                      </div>
                    </div>

                    <div class="row mt-4">
                      <div class="col-3">
                        <label for="email">Details</label>
                      </div>
                      <div class="col-9">
                        <textarea class="form-control" id="details" name="details" placeholder="Enter nominee's detail..." required>
                        </textarea>
                      </div>
                    </div>

                    <div class="row mt-4">
                      <div class="col-3">
                        <label for="email">Picture</label>
                      </div>
                      <div class="col-9">
                        <input type="file" name="pic" id="pic" class="form-control">
                        <!-- <input type="text" class="form-control" id="major" name="major" placeholder="Enter student's major..." required> -->
                      </div>
                    </div>

                    <div class="row mt-4">
                      <div class="col-3">
                        <label for="email">Nominee No.</label>
                      </div>
                      <div class="col-9">
                        <select class="form-select" id="nominee_no" name="nominee_no">
                          <option selected>Choose nominee no.</option>
                          <option value="01">01</option>
                          <option value="02">02</option>
                          <option value="03">03</option>
                          <option value="04">04</option>
                          <option value="05">05</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-12 mt-4 d-flex justify-content-end align-items-center">
                      <button class="more-btn" type="submit">Add Nominee <i class="bi bi-chevron-right"></i></button>
                      <!-- <button class="btn btn-dark mt-3 col-lg-12" type="submit">Register</button> -->
                    </div>
                  </div>
                </form>
          </div>

      </div>
    </section><!-- End Why Us Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <!--
          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Pemira FSM Undip 2022</h3>
            <p>
              Cyber 2 Tower 18th Floor, Jl. H. R. Rasuna Said Blok X-5, Kav. 13<br>
              Jakarta Selatan, DKI Jakarta 12950<br>
              Indonesia <br><br>
              <strong>Phone:</strong> +62 21 5799 8265<br>
              <strong>Email:</strong> <a href="mailto:hello@consultwithoptimus.id">hello@consultwithoptimus.id</a><br>
            </p>
          </div>
          -->

          <div class="col-lg-6 col-md-6 footer-links">
            <h4>Sitemap</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a class="scrollto" href="#hero">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/procedure">About Us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a class="scrollto" href="#all-about-pemira">All About Pemira</a></li>
              <!--
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
              -->
            </ul>
          </div>

          <!--
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>
          -->

          <!--
          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
          -->

        </div>
      </div>
    </div>

    @include('footer_public')