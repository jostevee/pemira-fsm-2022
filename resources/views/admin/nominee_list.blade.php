@include('header')
@php
  // $status = $data['status'];
  /*
  if(isset($data)){
    print($data['status']);
    foreach($data as $p){
      print($p);
    }
  }
  */
@endphp

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!-- <h1 class="logo me-auto"><a href="index.html">Medilab</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="#" class="logo me-auto"><img src="{{ asset ('assets/img/logo_optimus_navbar.webp') }}" alt="" class="img-fluid"><!-- <h4>Optimus</h4> --></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link" href="/">Home</a></li>
          <!-- <li><a class="nav-link scrollto" href="#procedure">Add Voter</a></li> -->
          <li class="dropdown"><a class="nav-link"><span>Student (Voter)</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a class="nav-link" href="/admin/student/list">List Student</a></li>
              <li><a class="nav-link" href="/admin/student/add">Add Student</a></li>
            </ul>
          </li>
          <li class="dropdown"><a class="nav-link active"><span>Nominee</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a class="nav-link" href="#">List Nominee</a></li>
              <li><a class="nav-link" href="/admin/nominee/add">Add Nominee</a></li>
            </ul>
          </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      <a href="/logout" class="appointment-btn scrollto">Logout <i class="bi bi-chevron-right"></i></a>
    </div>

  </header><!-- End Header -->


  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="col-12 acrylic justify-content-center">
      <div class="container">
        <h1>List Nominee</h1>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us mt-4">
      <div class="container">

        <div class="row">
          <div class="col-lg-12 align-items-stretch">
            <div class="content">
              <div class="card-body row d-flex justify-content-center">
                @if (session('error'))
                  <div class="alert alert-danger">
                    {{ session('error') }}
                  </div>
                @endif
                @if (session('success'))
                  <div class="alert alert-success">
                    {{ session('success') }}
                  </div>
                @endif
                <!-- <a href="/dashboard" class="mb-3 btn btn-primary">Dashboard</a> -->
                <div class="col-12 row">
                  <div class="col-6">
                    <a href="/admin/nominee/add" class="more-btn">Add Nominee <i class="bi bi-plus-circle-fill"></i></a>
                  </div>
                  <div class="col-6">
                    <p style="text-align: right;">Total Nominees = {{$nominee->count()}} </p>
                  </div>
                <table class="table table-dark table-hover">
                  <tr>
                    <th width="10%">No.</th>
                    <th width="15%">Name</th>
                    <th width="20%">Picture</th>
                    <th width="15%">Nominee No.</th>
                    <th width="30%">Details</th>
                  </tr>
                  @php
                    $id = 1;
                  @endphp
                  @forelse($nominee as $data)
                    <tr>
                      <td>{{$id++}}</td>
                      <td>
                        <a href="{{$data->id}}" style="color: #FFFFFF;">{{$data->name}}</a>
                      </td>
                      <td>
                        <img src="/assets/img/nominee_list/{{$data->pic}}"  alt="" class="img-fluid" style="border-radius: 20px;"/>
                      </td>
                      <td>{{$data->nominee_no}}</td>
                      <td>{{$data->details}}</td>
                      <!-- <td>
                        <a href="{{$data->id}}" class="btn btn-success col-12 my-1">Preview</a>
                        <a class="btn btn-warning mb-2 col-12" href="/writer/job/edit/{{$data->id}}">Edit</a>
                        <form action="delete/{{$data->id}}" method="POST">@csrf
                          <button class="btn btn-danger col-12" type="submit">Delete</button>
                        </form>
                      </td> -->
                    </tr>
                  @empty
                    <tr>
                      <td colspan="5" align="center">No data available</td>
                    </tr>
                  @endforelse
                </table>

                <!--
                <h3>Why do I have to cast my vote?</h3>
                <p>
                  We're proven to have a good track record in placing qualified talents to numerous kinds of industries. Preserving our clients’ trust thus able to
                  build a long-lasting relationship with them and value teamwork the most when it comes to delivering our services. Our works are the harmony
                  of youthful, energetic, and experienced professionals. We offer our clients a devoted team to provide valuable advice and insights that are
                  needed to face every challenge in human capital. We understand the importance of clients’ satisfaction with the service we offer and putting
                  flexibility as the bedrock of our process.
                </p>
                -->
              
              </div>
          </div>

          <!--
          <div class="col-lg-8 align-items-stretch"><!-- d-flex
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch" onclick="location.href='about-us';" style="cursor: pointer;">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-receipt"></i>
                    <h4>About Us</h4>
                    <!-- <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                  </div>
                </div>

                <div class="col-xl-4 d-flex align-items-stretch" onclick="location.href='our-solutions';" style="cursor: pointer;">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-cube-alt"></i>
                    <h4>Our Solutions</h4>
                    <!-- <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                  </div>
                </div>

                <div class="col-xl-4 d-flex align-items-stretch" onclick="location.href='events';" style="cursor: pointer;">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-images"></i>
                    <h4>Events</h4>
                    <!-- <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p> 
                  </div>
                </div>
              </div>
            </div><!-- End .content
          </div>
        </div>-->

      </div>
    </section><!-- End Why Us Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <!--
          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Pemira FSM Undip 2022</h3>
            <p>
              Cyber 2 Tower 18th Floor, Jl. H. R. Rasuna Said Blok X-5, Kav. 13<br>
              Jakarta Selatan, DKI Jakarta 12950<br>
              Indonesia <br><br>
              <strong>Phone:</strong> +62 21 5799 8265<br>
              <strong>Email:</strong> <a href="mailto:hello@consultwithoptimus.id">hello@consultwithoptimus.id</a><br>
            </p>
          </div>
          -->

          <div class="col-lg-6 col-md-6 footer-links">
            <h4>Sitemap</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a class="scrollto" href="#hero">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/procedure">About Us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a class="scrollto" href="#all-about-pemira">All About Pemira</a></li>
              <!--
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
              -->
            </ul>
          </div>

          <!--
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>
          -->

          <!--
          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
          -->

        </div>
      </div>
    </div>

    @include('footer_public')