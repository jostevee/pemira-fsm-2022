@include('header')
@php
  $status = $data['status'];
  /*
  if(isset($data)){
    print($data['status']);
    foreach($data as $p){
      print($p);
    }
  }
  */
@endphp

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!-- <h1 class="logo me-auto"><a href="index.html">Medilab</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="#" class="logo me-auto"><img src="{{ asset ('assets/img/logo_optimus_navbar.webp') }}" alt="" class="img-fluid"><!-- <h4>Optimus</h4> --></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto" href="#hero">Home</a></li>
          @if($status == 0 || $status == 1)
          <li><a class="nav-link scrollto" href="#procedure">Procedure</a></li>
          <li class="dropdown"><a class="nav-link scrollto" href="#all-about-pemira"><span>All About Pemira</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a class="nav-link scrollto" href="#all-about-pemira">Final Voter List</a></li>
              <li><a class="nav-link scrollto" href="#all-about-pemira">Polling Station</a></li>
              <li><a class="nav-link scrollto" href="#all-about-pemira">Nominees</a></li>
            </ul>
          </li>
          @elseif($status == 2)
          <!-- <li><a class="nav-link scrollto" href="#procedure">Add Voter</a></li> -->
          <li class="dropdown"><a class="nav-link"><span>Student (Voter)</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a class="nav-link" href="/admin/student/list">List Student</a></li>
              <li><a class="nav-link" href="/admin/student/add">Add Student</a></li>
            </ul>
          </li>
          <li class="dropdown"><a class="nav-link"><span>Nominee</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a class="nav-link" href="/admin/nominee/list">List Nominee</a></li>
              <li><a class="nav-link" href="/admin/nominee/add">Add Nominee</a></li>
            </ul>
          </li>
          @endif
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      @if($status == 0)
      <a href="/login" class="appointment-btn scrollto">Cast My Vote!</a>
      @elseif($status == 1 || $status == 2)
      <a href="/logout" class="appointment-btn scrollto">Logout <i class="bi bi-chevron-right"></i></a>
      @endif
      
      <!-- <i class="bx bx-envelope"></i> -->
      <!-- <a href="#appointment" class="appointment-btn scrollto"><span class="d-none d-md-inline">Make an</span> Appointment</a> -->

    </div>

  </header><!-- End Header -->


  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="col-12 acrylic">
      <div class="container">
      @if($status == 0)
        <h1>Welcome to Pemira FSM 2022</h1>
        <h2>Satu Suara untuk Satu Tujuan, FSM Jaya!</h2>
      @elseif($status == 1 || $status == 2)
        <h1>Welcome,</h1>
        <h2>{{($data['logged_user'])['name']}} | {{($data['logged_user'])['nim']}}</h2>
      @endif
      </div>
      <!-- <a href="#about" class="btn-get-started scrollto">About Us</a> -->
    </div>
  </section><!-- End Hero -->

  <!-- ======= Recent Events Section ======= -->
  <!-- <section id="hero" class="recent-events"> -->
    <!-- <div class="container"> -->

      <!-- IF FIRST CONDITION -->
        <!-- <h1 style="margin-top: 250px;">Welcome to Optimus</h1> -->
        <!-- <h2>Discover Technology Based HR Solution</h2> -->
      
      <!-- ELSE CONDITION -->
        <!-- <div class="recent-events-slider swiper-container"> -->
        <!-- <div class="swiper-wrapper d-flex align-items-center">justify-content-center -->
          
          <!-- FOREACH CONDITION -->
          <!-- <div class="swiper-slide"> -->
            <!-- <div class="recent-event-wrap"> -->
              <!-- <div class="recent-event-item"> -->

                <!-- <img src="{{ asset ('assets/img/testimonials/testimonials-1.jpg') }}" class="recent-event-img" alt=""> -->
                <!-- <img src="../optimus_consulting_admin/img/" class="recent-event-img" alt=""> -->
                <!-- <h3></h3> -->

                <!-- <a href="event/" class="galelry-lightbox"><img src="/img/event_list/" class="recent-event-img" alt="gambar"></a> -->
                <!-- <a href="/events"><img src="/assets/img/event_list/" class="recent-event-img" alt="gambar"></a> -->

                <!-- <h4>Ceo &amp; Founder</h4> -->
                <!--
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                {{implode(' ', array_slice(explode(' ', 'aku mau makan bersama dengan temanku yang sangat baik hati dan suka berbagi'), 0, 5))."\n"}}
                <i class="bx bxs-quote-alt-right quote-icon-right"></i> -->

              <!-- </div> -->
            <!-- </div> -->
          <!-- </div> --><!-- End recent event item -->
        
        <!-- ENDFOREACH CONDITION -->

        <!--
          <div class="swiper-slide">
            <div class="recent-event-wrap">
              <div class="recent-event-item">
                <img src="{{ asset ('assets/img/testimonials/testimonials-2.jpg') }}" class="recent-event-img" alt="">
                <h3>Sara Wilsson</h3>
                <h4>Designer</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div>
          </div><!-- End recent event item

          <div class="swiper-slide">
            <div class="recent-event-wrap">
              <div class="recent-event-item">
                <img src="{{ asset ('assets/img/testimonials/testimonials-3.jpg') }}" class="recent-event-img" alt="">
                <h3>Jena Karlis</h3>
                <h4>Store Owner</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div>
          </div><!-- End recent event item

          <div class="swiper-slide">
            <div class="recent-event-wrap">
              <div class="recent-event-item">
                <img src="{{ asset ('assets/img/testimonials/testimonials-4.jpg') }}" class="recent-event-img" alt="">
                <h3>Matt Brandon</h3>
                <h4>Freelancer</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div>
          </div><!-- End recent event item

          <div class="swiper-slide">
            <div class="recent-event-wrap">
              <div class="recent-event-item">
                <img src="{{ asset ('assets/img/testimonials/testimonials-5.jpg') }}" class="recent-event-img" alt="">
                <h3>John Larson</h3>
                <h4>Entrepreneur</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div>
          </div><!-- End recent event
        -->

        <!-- </div> -->
        <!-- <div class="swiper-pagination"></div> -->
        <!-- </div> -->

      <!-- ENDIF CONDITION -->
    <!-- </div> -->
  <!-- </section> --> <!-- End Testimonials Section -->

  <main id="main">

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us mt-4">
      <div class="container">

        <div class="row">
          <div class="col-lg-12 align-items-stretch">
            <div class="content">

              @if($status == 0)
                <h3>Why do I have to cast my vote?</h3>
                <p>
                  We're proven to have a good track record in placing qualified talents to numerous kinds of industries. Preserving our clients’ trust thus able to
                  build a long-lasting relationship with them and value teamwork the most when it comes to delivering our services. Our works are the harmony
                  of youthful, energetic, and experienced professionals. We offer our clients a devoted team to provide valuable advice and insights that are
                  needed to face every challenge in human capital. We understand the importance of clients’ satisfaction with the service we offer and putting
                  flexibility as the bedrock of our process.
                </p>
                <div><!-- class="text-center" -->
                  <a href="/login" class="more-btn">Cast your vote, NOW! <i class="bx bx-chevron-right"></i></a>
                </div>
              @elseif($status == 1)
                @if (session('error'))
                  <div class="alert alert-danger">
                    {{ session('error') }}
                  </div>
                @elseif(session('success'))
                  <div class="alert alert-success">
                    {{ session('success') }}
                  </div>
                @endif
                <div class="row justify-content-center services">
                  @forelse($data['nominees'] as $nominee)
                      <div class="col-lg-3 col-md-3 m-4 mt-md-0 mt-lg-0" data-aos="fade-up" data-aos-delay="100" data-aos-duration="700">
                      <!-- mt-4 -->
                        <div class="icon-box row d-flex align-items-stretch justify-content-center">
                          <img src="/assets/img/nominee_list/{{$nominee->pic}}" onclick="location.href='student/vote/{{$nominee->id}}';" style="cursor: pointer;" alt="" class="img-fluid" style="border-radius: 20px;"/>
                          <h4><a>{{$nominee->name}}</a></h4>
                          <h3><a>{{$nominee->nominee_no}}</a></h3>
                        </div>
                      </div>
                  @empty
                    <h1 style="text-align: center;">Election Day coming soon!</h1>
                  @endforelse
                </div>
              @elseif($status == 2)
                @php
                  $nominee = $data['nominee'];
                  $vote_results = $data['vote_results']->groupby('id_nominee')->sortByDesc('id_nominee');
                @endphp

                <div class="row justify-content-center services">
                  @foreach($vote_results as $key => $result)
                    @foreach($nominee as $data)
                      @if($data['id'] == $key)
                          <div class="col-lg-3 col-md-3 m-4 mt-md-0 mt-lg-0" data-aos="fade-up" data-aos-delay="100" data-aos-duration="700">
                          <!-- mt-4 -->
                            <div class="icon-box row d-flex align-items-stretch justify-content-center">
                              <h4><a>{{$data->name}}</a></h4>
                              <h4><a>Votes: {{$result->count()}}</a></h4>
                            </div>
                          </div>
                      @endif
                    @endforeach
                  @endforeach
                </div>

              @endif
            </div>
          </div>

          <!--
          <div class="col-lg-8 align-items-stretch"><!-- d-flex
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch" onclick="location.href='about-us';" style="cursor: pointer;">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-receipt"></i>
                    <h4>About Us</h4>
                    <!-- <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                  </div>
                </div>

                <div class="col-xl-4 d-flex align-items-stretch" onclick="location.href='our-solutions';" style="cursor: pointer;">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-cube-alt"></i>
                    <h4>Our Solutions</h4>
                    <!-- <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                  </div>
                </div>

                <div class="col-xl-4 d-flex align-items-stretch" onclick="location.href='events';" style="cursor: pointer;">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-images"></i>
                    <h4>Events</h4>
                    <!-- <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p> 
                  </div>
                </div>
              </div>
            </div><!-- End .content
          </div>
        </div>-->

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Procedure Section ======= -->
    <section id="procedure" class="about">
      <div class="container">

        <div class="section-title">
          <h2>About Us</h2>
          <p data-aos="fade-down" data-aos-delay="100" data-aos-duration="700">
            With proper end-to-end recruitment strategy done by our professionals
            and assisted by impressive digital technology support, finding the right
            talent for your business has never been so enjoyable. We are eager to cordially
            assist you to find the perfect match for your business: job fit, culture fit,
            and budget fit. We offer you, both clients and candidates, a flexible yet
            accountable recruitment and HR solutions, tailored to meet the needs of any
            kind of industry.
          </p>
          <a href="about-us" class="btn-get-started scrollto">Learn More <i class="bx bx-chevron-right"></i></a>
        </div>

      </div>

      <!--
      <div class="container-fluid" id="values">

        <div class="row">
          <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch position-relative">
            <a href="https://youtu.be/oBt-nGvTBJM?t=470" class="glightbox play-btn mb-4"></a>
          </div>

          <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
            <h3>Our Values</h3>
            <div class="icon-box" data-aos="fade-right" data-aos-delay="100" data-aos-duration="700">
              <div class="icon"><i class="bx bx-check-shield"></i></div>
              <h4 class="title"><a href="">Trusted</a></h4>
              <p class="description">
                We have proven a good track record in placing qualified talents to numerous kinds of
                industries, namely BUMN, FMCG, and FSI companies. On top of that, we are able to
                preserve our clients’ trust thus able to build a long-lasting relationship with them.
              </p>
            </div>

            <div class="icon-box" data-aos="fade-right" data-aos-delay="100" data-aos-duration="700">
              <div class="icon"><i class="bx bx-group"></i></div>
              <h4 class="title"><a href="">Teamwork</a></h4>
              <p class="description">
                Teamwork makes the dream work. We value teamwork the most when it comes to
                delivering our services. Our works are the harmony of youthful, energetic, and
                experienced professionals.
              </p>
            </div>

            <div class="icon-box" data-aos="fade-right" data-aos-delay="100" data-aos-duration="700">
              <div class="icon"><i class="bx bx-target-lock"></i></div>
              <h4 class="title"><a href="">Committed</a></h4>
              <p class="description">
                We offer our clients a devoted team to provide valuable advice and insights
                that are needed to face every challenge in human capital.
              </p>
            </div>

            <div class="icon-box" data-aos="fade-right" data-aos-delay="100" data-aos-duration="700"> 
              <div class="icon"><i class="bx bx-shuffle"></i></div>
              <h4 class="title"><a href="">Flexible</a></h4>
              <p class="description">
                We understand the importance of clients’ satisfaction with the service we
                offer. On this account, we put flexibility as the bedrock of our process.
              </p>
            </div>

            <div class="icon-box" data-aos="fade-right" data-aos-delay="100" data-aos-duration="700">
              <div class="icon"><i class="fa fa-heartbeat"></i></div>
              <h4 class="title"><a href="">Lively</a></h4>
              <p class="description">
                On top of that, we are aspiring to be your partner every step of the way until
                you find your perfect suit and we are planning on doing that enjoyably.
              </p>
            </div>

          </div>
        </div>

      </div>
      <section class="team">
      <div class="container" id="meet-the-team">

        <div class="section-title">
          <h2>Meet the Team</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">

          <div class="col-lg-6 mt-4" data-aos="fade-right" data-aos-delay="100" data-aos-duration="700">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{ asset ('assets/img/team/Dien-Natalie-150.jpg') }}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Dien Natalia</h4>
                <span>Founder and Managing Director</span>
                <p style="text-align: justify;">
                <!-- style="text-align: justify;"
                  Dien started Optimus with over a decade of remarkable experience in human capital
                  and...
                </p>
                <div class="learn-more">
                  <a id="edit" class="more-btn" data-toggle="modal" data-remote="true">Learn More <i class="bx bx-chevron-right"></i></a>
                </div>
                <div class="social">
                  <a href="#"><i class="ri-twitter-fill"></i></a>
                  <a href="#"><i class="ri-facebook-fill"></i></a>
                  <a href="#"><i class="ri-instagram-fill"></i></a>
                  <a href="#"> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <!-- IT industries. Her expertise includes recruitment, training, assessment, and
                  outsourcing. Settling the longest period in the field of sales in numbers of
                  national HR companies, she led her teams to meet the needs of her then-happy
                  clients. Her current teammates value her integrity and her habit of cherishing
                  people around her.

          <div class="col-lg-6 mt-4" data-aos="fade-left" data-aos-delay="100" data-aos-duration="700">
          <!-- mt-lg-0 mt-md-0 
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{ asset ('assets/img/team/Raymond-Koh-150.jpg') }}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Raymond Koh</h4>
                <span>Co-Founder</span>
                <p style="text-align: justify;">
                  Raymond is a young-outgoing co-founder of Optimus. Currently, he also acts as
                  a Client Account Manager...
                </p>
                <div class="learn-more">
                  <a id="edit" class="more-btn" data-toggle="modal" data-remote="true">Learn More <i class="bx bx-chevron-right"></i></button>
                </div>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <!-- Always interested in HR and technology, Raymond
                  started his career as a LinkedIn Talent Solution associate and account executive
                  in an HR recruitment company. He then gained some international work experience
                  with his former IT solutions company. He then joined Dien Natalia establishing
                  Pemira FSM Undip 2022. Until now, he is really into his dynamic work-life at Optimus.

          <div class="col-lg-6 mt-4" data-aos="fade-right" data-aos-delay="100" data-aos-duration="700">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{ asset ('assets/img/team/Lucie-Supandi-150.jpg') }}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Lucie Supandi</h4>
                <span>Head of Consultant</span>
                <p style="text-align: justify;">
                  Specialized in the end-to-end recruitment process and headhunting,
                  Lucie has been in this industry for more than 15 years...
                </p>
                <div class="learn-more">
                  <a id="edit" class="more-btn" data-toggle="modal" data-remote="true">Learn More <i class="bx bx-chevron-right"></i></button>
                </div>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <!-- Living abroad for
                  more than a decade, she started her career in HR with a well-known hotel
                  in the USA. Returning to Jakarta, she was assigned as head consultant and
                  executive search assisted in various industries. Her other exposure was managing
                  Recruitment Process Outsourcing for a leading telecommunication, O/G, banking,
                  startup, FMCG, and property management. In her work, Lucie is driven by her love
                  studying humans and the joy she gets from putting the right person in the right
                  place. Her broad range of clienteles and her national and overseas experiences
                  have shaped her into who she is now.

          <div class="col-lg-6 mt-4" data-aos="fade-left" data-aos-delay="100" data-aos-duration="700">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{ asset ('assets/img/team/Toman-Alberto-150.jpg') }}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Toman Alberto</h4>
                <span>Head of PMO and Partnership</span>
                <p style="text-align: justify;">
                  Toman is a relatively newcomer in the human capital industry. Graduated
                  with an engineering degree from ITB, Toman has 10 years’ experience in
                  the oil and gas industry...
                </p>
                <div class="learn-more">
                  <a id="edit" class="more-btn" data-toggle="modal" data-remote="true">Learn More <i class="bx bx-chevron-right"></i></button>
                </div>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <!-- In 2017, Toman started his journey in one of the
                  HR-tech startups in Indonesia. This exposure to the “new world” had this
                  eye-opening effect on him: he came to realize that the world is so full of
                  opportunities that need to be explored. His grit and curious traits are what
                  brought him to this far: enjoying the whale of a time in Pemira FSM Undip 2022. 

        </div>

      </div>
      </section>
      -->

    </section><!-- End About Section -->

    <!-- ======= All About Pemira Section ======= -->
    <section id="all-about-pemira" class="services">
      <div class="container">

        <div class="section-title">
          <h2>All About Pemira</h2>
          <!--
          <p data-aos="fade-down" data-aos-delay="100" data-aos-duration="700">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
          -->
        </div>

        <div class="row justify-content-center">
          <div class="col-lg-3 col-md-3 m-4 mt-md-0 mt-lg-0" data-aos="fade-down" data-aos-delay="100" data-aos-duration="700">
          <!-- mt-4 -->
            <div class="icon-box row d-flex align-items-stretch justify-content-center" onclick="location.href='recruitment-services';" style="cursor: pointer;">
              <div class="icon"><i class="fas fa-users"></i></div>
              <h4><a>Final Voter List</a></h4>
              <span data-purecounter-start="0" data-purecounter-end="1200" data-purecounter-duration="20" class="purecounter"></span>
              <p>
                Voters
              </p>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 m-4 mt-md-0 mt-lg-0" data-aos="fade-down" data-aos-delay="100" data-aos-duration="700">
          <!-- mt-md-0 -->
            <div class="icon-box row d-flex align-items-stretch justify-content-center" onclick="location.href='online-assessment';" style="cursor: pointer;">
              <div class="icon"><i class="fas fa-users"></i></div>
                <h4><a>Polling Station</a></h4>
                <span data-purecounter-start="0" data-purecounter-end="4" data-purecounter-duration="20" class="purecounter"></span>
                <p>
                  location
                </p>
              </div>
          </div>

          <div class="col-lg-3 col-md-3 m-4 mt-md-0 mt-lg-0" data-aos="fade-down" data-aos-delay="100" data-aos-duration="700">
          <!-- mt-4 -->
            <div class="icon-box row d-flex align-items-stretch justify-content-center" onclick="location.href='hr-consultancy';" style="cursor: pointer;">
              <div class="icon"><i class="fas fa-users"></i></div>
                <h4><a>Nominees</a></h4>
                <span data-purecounter-start="0" data-purecounter-end="3" data-purecounter-duration="20" class="purecounter"></span>
                <p>
                  candidates
                </p>
              </div>
          </div>
        </div>

      </div>
    </section><!-- End Our Solutions Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <!--
          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Pemira FSM Undip 2022</h3>
            <p>
              Cyber 2 Tower 18th Floor, Jl. H. R. Rasuna Said Blok X-5, Kav. 13<br>
              Jakarta Selatan, DKI Jakarta 12950<br>
              Indonesia <br><br>
              <strong>Phone:</strong> +62 21 5799 8265<br>
              <strong>Email:</strong> <a href="mailto:hello@consultwithoptimus.id">hello@consultwithoptimus.id</a><br>
            </p>
          </div>
          -->

          <div class="col-lg-6 col-md-6 footer-links">
            <h4>Sitemap</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a class="scrollto" href="#hero">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/procedure">About Us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a class="scrollto" href="#all-about-pemira">All About Pemira</a></li>
              <!--
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
              -->
            </ul>
          </div>

          <!--
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>
          -->

          <!--
          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
          -->

        </div>
      </div>
    </div>

    @include('footer_public')