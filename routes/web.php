<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\StudentController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// FrontEnd Controller
Route::get('/', function () {return redirect('/home');});
Route::get('/home', [FrontEndController::class, 'home']);

// Login landing page
Route::get('/panel', [FrontEndController::class, 'landingLogin'])->name('not-login');

// Student's Auth
Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('/login', [LoginController::class, 'auth']);
// Route::get('/signup', [LoginController::class, 'signupStudent']);
// Route::post('/signup', [LoginController::class, 'registerStudent']);
Route::get('/logout', [LoginController::class, 'logout']);

// Student
Route::middleware('auth:web')->group(function () {
    Route::get('/student', [StudentController::class, 'index']);
    Route::get('/student/vote/{id}', [StudentController::class, 'voteNominee']);
    // Route::get('/student/event/list', [StudentController::class, 'myEvent']);
});

// Admin Auth
// Route::get('/admin/login', [LoginController::class, 'loginAdmin']); //->name('admin.login');
// Route::post('/admin/login', [LoginController::class, 'authAdmin']);
Route::get('/admin/register', [LoginController::class, 'registerViewAdmin']);
Route::post('/admin/register', [LoginController::class, 'registerSubmitAdmin']);
// Route::get('/admin/logout', [LoginController::class, 'logoutAdmin']);

// Admin
Route::middleware('auth:admin')->group(function () {
    Route::get('/admin', [AdminController::class, 'index']);
    Route::get('/admin/student/list', [AdminController::class, 'listStudent']);
    Route::get('/admin/student/add', [AdminController::class, 'addViewStudent']);
    Route::post('/admin/student/add', [AdminController::class, 'addSubmitStudent']);
    Route::get('/admin/nominee/list', [AdminController::class, 'listNominee']);
    Route::get('/admin/nominee/add', [AdminController::class, 'addViewNominee']);
    Route::post('/admin/nominee/add', [AdminController::class, 'addSubmitNominee']);

    // Route::get('/admin/student/register', [AdminController::class, 'registerViewStudent']);
});