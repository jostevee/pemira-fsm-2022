<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoteResult extends Model
{
    use HasFactory;

    protected $table = 'vote_results';
    protected $primaryKey = 'id';

    public function nominee() {
        return $this->belongsTo('App\Models\Nominee', 'id_nominee');
    }

    public function student() {
        return $this->belongsTo('App\Models\Student', 'id_student');
    }
}
