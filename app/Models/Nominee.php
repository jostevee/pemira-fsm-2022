<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nominee extends Model
{
    use HasFactory;

    protected $table = 'nominees';
    protected $primaryKey = 'id';

    public function vote_results(){
        return $this->hasMany('App\Models\VoteResult', 'id_nominee');
    }
}
