<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Student;
use App\Models\VoteResult;
use App\Models\Nominee;

// use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    // INDEX
    public function index() {
        // $event = Category::orderByDesc('created_at')->get();
        if(Auth::guard('web')->user() != null){
            $data['logged_user'] = Student::where('id', Auth::guard('web')->user()->id)->firstOrFail();
            $data['status'] = 1;
        } elseif (Auth::guard('admin')->user() != null){
            $data['logged_user'] = Admin::where('id', Auth::guard('admin')->user()->id)->firstOrFail();
            $data['status'] = 2;
        }
        $data['vote_results'] = VoteResult::all();
        $data['nominee'] = Nominee::all();

        return view('home', compact('data'));
    }


    // Student
    public function addViewStudent(){
        return view('/admin/student_add');
    }

    public function addSubmitStudent(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'nim' => 'required|unique:students|min:14|max:14',
            'major' => 'required',
            'year' => 'required',
            // 'password' => 'required|min:6',
        ]);

        $password = Str::random(6);
        $student = new Student();
        $student->nim = $request->nim;
        $student->name = $request->name;
        $student->major = $request->major;
        $student->year = $request->year;
        $student->token = $password;
        $student->password = Hash::make($password);
        $student->save();

        return redirect('/admin/student/list')->with('success', 'Add '.$request->name.' - '.$request->nim.' successful.');
    }

    /*
    public function registerViewStudent(){
        if(Auth::guard('admin')->user() == null){
            return redirect()->intended('/login');
        }
        return view('/admin/student_register');
    }

    public function registerSubmitStudent(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'nim' => 'required|unique:students|min:14|max:14',
            'major' => 'required',
            'year' => 'required',
            // 'password' => 'required|min:6',
        ]);

        $student = new Student();
        $student->nim = $request->nim;
        $student->name = $request->name;
        $student->password = Hash::make(str_random(6));
        $student->save();

        return redirect('/login')->with('success', 'Registration process successful, please sign in');
    }
    */

    // List 
    public function listStudent(){
        $student = Student::all();
        return view('admin/student_list', compact('student'));
    }


    // Nominee
    public function addViewNominee(){
        return view('/admin/nominee_add');
    }

    public function addSubmitNominee(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'nominee_no' => 'required',
            'details' => 'required',
            // 'pic'=> 'required|mimes:png,jpg,jpeg,webp',
            // 'password' => 'required|min:6',
        ]);

        $nominee = new Nominee();
        $nominee->name = $request->name;
        $nominee->details = $request->details;
        $nominee->nominee_no = $request->nominee_no;        
        if ($request->hasFile('pic')) {
            $picture = $request->file('pic');
            $fileName = time().$picture->getClientOriginalName();

            // Create 'img/event_list' directory if it isn't available yet
            if(!is_dir('assets/img/nominee_list')) {
                mkdir('assets/img/nominee_list', 0777, true);
            }

            // Move the file to the selected directory
            $request->file('pic')->move('assets/img/nominee_list', $fileName);
            $nominee->pic = $fileName;
            $nominee->save();
            return redirect('/admin/nominee/list')->with('success', 'Add '.$nominee->name.' - '.$request->nominee_no.' successful.');
        } else {
            $nominee->pic = '';
            return redirect('/admin/nominee/list')->with('error', 'Add '.$nominee->name.' - '.$request->nominee_no.' error.');
        }

    }

    public function listNominee(){
        $nominee = Nominee::all();
        return view('admin/nominee_list', compact('nominee'));
    }


    public function addCategory(){
        return view('admin/add_category');
    }

    public function listCategory(){
        $category = Category::all();
        return view('admin/list_category', compact('category'));
    }

    public function saveCategory(Request $request){
        $category = new Category();
        $category->category = $request->input('name');

        $category->save();

        return redirect('/admin/category/list/');
    }

    public function deleteCategory($id){
        $category = Category::where('id', $id)->firstOrFail();

        $category->delete();

        return redirect()->back()->with('success', 'Selected category successfully deleted');

    }

    public function editCategory($id){
        $data = Category::where('id', $id)->firstOrFail();

        return view('admin/edit_category', compact('data'));

    }

    public function updateCategory(Request $request, $id){
        $category =  Category::where('id', $id)->firstOrFail();
        $category->name = $request->input('name');

        $category->save();

        return redirect('/admin/category/list');
    }

    public function addCareerCategory(){

        return view('admin/add_career_category');
    }

    public function listCareerCategory(){
        $category = CareerCategory::all();
        return view('admin/list_career_category', compact('category'));
    }

    public function saveCareerCategory(Request $request){
        $category = new CareerCategory();
        $category->category = $request->input('name');

        $category->save();

        return redirect('/admin/job/category/list/');
    }

    public function deleteCareerCategory($id){
        $category = CareerCategory::where('id', $id)->firstOrFail();

        $category->delete();

        return redirect()->back()->with('success', 'Selected category successfully deleted');

    }

    public function editCareerCategory($id){
        $data = CareerCategory::where('id', $id)->firstOrFail();

        return view('admin/edit_career_category', compact('data'));

    }

    public function updateCareerCategory(Request $request, $id){
        $category =  CareerCategory::where('id', $id)->firstOrFail();
        $category->category = $request->input('name');

        $category->save();

        return redirect('/admin/job/category/list');
    }

    public function resetPassword($id) {
        $data = Writer::where('id', $id)->firstOrFail();

        return view('admin/reset_pass', compact('data'));
    }

    public function storeresetPassword(Request $request, $id){
        $data = Writer::where('id', $id)->firstOrFail();

        if($request->input('password')!== null || $request->input('passwordconfirm') !== null) {
            if (($request->input('password') == $request->input('passwordconfirm'))){
                $data->password = Hash::make($request->input('password'));
            } else {
                return redirect()->back()->with('error', 'Please check your Confirmation Password!');
            }
        }

        $data->save();

        return redirect('/admin/writer/list/')->with('success', 'Your Password has been successully reset');
    }
}
