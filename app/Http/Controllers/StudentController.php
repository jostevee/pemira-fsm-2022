<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nominee;
use App\Models\VoteResult;
use App\Models\Student;

// use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    public function index(){
        if(Auth::guard('web')->user() != null){
            $data['logged_user'] = Student::where('id', Auth::guard('web')->user()->id)->firstOrFail();
            $data['status'] = 1;
        } elseif (Auth::guard('admin')->user() != null){
            $data['logged_user'] = Admin::where('id', Auth::guard('admin')->user()->id)->firstOrFail();
            $data['status'] = 2;
        }
        $data['nominees'] = Nominee::all();

        return view('home', compact('data'));
    }


    // Voting
    public function voteNominee($id){
        // return redirect('/');
        $id_authuser = Auth::guard('web')->user()->id;
        $data = VoteResult::where('id_student', $id_authuser)->get();
        if($data->count() == 0){
            $vote_result = new VoteResult();
            $vote_result->id_student = Auth::guard('web')->user()->id;
            $vote_result->id_nominee = $id;
            $vote_result->save();
            return redirect()->back()->with('success', 'Thank you for your vote!');
        } else {
            return redirect()->back()->with('error', 'You already cast your vote!');
        }
    }

    
    // Account
    public function editAccount($id){
        $data = Writer::where('id', $id)->firstOrFail();

        if ($data->id == Auth::guard('web')->user()->id_writer) {
            return view('writer/edit_account', compact('data'));
        }
        return redirect('/writer/editAccount/'.Auth::guard('web')->user()->id_writer);

    }
    
    public function updateAccount(Request $request, $id){
        $data = Writer::where('id', $id)->firstOrFail();
        if($data->id == Auth::guard('web')->user()->id_writer) {
            $data->name = $request->input('nama');

            if($request->input('oldpassword') !== null || $request->input('password')!== null || $request->input('passwordconfirm') !== null) {
                if (Hash::check($request->input('oldpassword'), $data->password) && ($request->input('password') == $request->input('passwordconfirm'))){
                    $data->password = Hash::make($request->input('password'));
                } else {
                    return redirect()->back()->with('error', 'Password Lama, Password Baru, atau konfirmasi password tidak tepat.');
                }
            }

            $data->save();

        }

        return redirect('/writer/editAccount/'.$data->id_writer)->with('success', 'Profile berhasil diupdate');
    }

    
    // EVENTS
    public function lookArticle($id){
        $data = Article::where('id', $id)->firstOrFail();

        return view('writer/look_single_article', compact('data'));
    }

    public function addArticle(){
        $category = Category::all();

        return view('writer/add_article', compact('category'));
    }

    public function saveArticle(Request $request){
        $article = new Article();
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        // $article->link = $request->input('link');
        // $article->id_category = $request->input('category');
        $article->id_writer = Auth::guard('web')->user()->id;

        if ($request->hasFile('img_link')) {
            $gambar = $request->file('img_link');
            $fileName = time().$gambar->getClientOriginalName();

            // Create 'img/event_list' directory if it isn't available yet
            if(!is_dir('assets/img/article_list')) {
                mkdir('assets/img/article_list', 0777, true);
            }

            // Move the file to the selected directory
            $request->file('img_link')->move('assets/img/article_list', $fileName);
            $article->img_link = $fileName;
        } else {
            $article->img_link = '';
        }
        $article->save();

        return redirect('/writer/article/'.$article->id);
    }

    public function editArticle($id){
        $data = Article::where('id', $id)->firstOrFail();

        if ($data->id_writer == Auth::guard('web')->user()->id) {
            return view('writer/edit_single_article', compact('data'));
        }
        return redirect('/writer/article/list');

    }

    public function updateArticle(Request $request, $id){
        $article =  Article::where('id', $id)->firstOrFail();
        if ($article->id_writer == Auth::guard('web')->user()->id) {
            $article->title = $request->input('title');
            $article->content = $request->input('content');
            // $article->link = $request->input('link');

            /*
            if ($request->input('img_link_update') != null) {
                if ($request->hasFile('img_link_update')) {
                    $event->img_link = '';
                    $gambar = $request->file('img_link_update');
                    $fileName = time().$gambar->getClientOriginalName();

                    if(!is_dir('img')) {
                        mkdir('img', 0777, true);
                    }
                    $request->file('img_link_update')->move('img/', $fileName);

                    $event->img_link = $fileName;
                } else {
                    $event->img_link = '';
                }
            }
            */

            $article->save();
        }

        return redirect('/writer/article/'.$article->id);
    }

    public function deleteArticle(Request $request, $id){
        $article =  Article::where('id', $id)->firstOrFail();
        if($article->id_writer == Auth::guard('web')->user()->id) {
            $article->delete();

            return redirect('/writer/article/list')->with('success', 'Selected article succesfully deleted');
        }

    }

    public function myArticle(){
        $article = Article::where('id_writer', Auth::guard('web')->user()->id)->orderByDesc('created_at')->get();

        return view('writer/my_article', compact('article'));
    }
}
