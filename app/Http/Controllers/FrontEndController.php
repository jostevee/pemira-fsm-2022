<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Results;
use Auth;

class FrontEndController extends Controller
{
    // Login landing page
    public function landingLogin() {
        return view('home');
    }

    // This is a controller for all of Pemira's pages
    public function home(){
        // $data['status'] = 0;
        // $event = Event::orderByDesc('created_at')->take(4)->get();
        // $social_media = SocialMedia::where('id', 1)->firstOrFail();
        // return view('/home', compact('data')); //, compact('event', 'social_media')


        if(Auth::guard('web')->user() != null){
            return redirect('/student');
        } elseif (Auth::guard('admin')->user() != null) {
            return redirect('/admin');
        } else {
            $data['status'] = 0;
            return view('/home', compact('data')); //, compact('event', 'social_media')
        }
    }

    public function aboutUs(){
        return view('about_us'); //, compact('data')
    }

    public function brewingSoon(){
        return view('brewingsoon'); //, compact('data')
    }
}
