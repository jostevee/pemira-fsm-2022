<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Writer;
use App\Models\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Auth;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use function Symfony\Component\HttpFoundation\get;

class LoginController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->middleware('guest:admin')->except('logoutAdmin', 'logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:web')->except('logout');
    }

    public function login(){
        return view('login');
    }

    public function auth(Request $request){
        if(Auth::guard('web')->user() != null){
            return redirect()->intended('/student');
        } elseif (Auth::guard('admin')->user() != null){
            return redirect()->intended('/admin');
        } elseif (Auth::guard('web')->attempt(['nim' => $request->nim, 'password' => $request->password])) {
            return redirect()->intended('/student');
        } elseif (Auth::guard('admin')->attempt(['nim' => $request->nim, 'password' => $request->password])) {
            return redirect()->intended('/admin');
        }

        return redirect('/login')->with('error','Invalid login credentials');
    }

    /*
    public function loginAdmin(){
        return view('admin/login');
    }
    */

    /*
    public function auth(Request $request){
        if(Auth::guard('web')->user() != null){
            return redirect()->intended('/student');
        } elseif (Auth::guard('web')->attempt(['user_id' => $request->user_id, 'password' => $request->password])) {
            return redirect()->intended('/student');
        }

        return redirect('/login')->with('error','Invalid login credentials');
    }

    public function authAdmin(Request $request){
        if(Auth::guard('admin')->user() != null){
            return redirect()->intended('/admin');
        } elseif (Auth::guard('admin')->attempt(['user_id' => $request->user_id, 'password' => $request->password])) {
            return redirect()->intended('admin');
        }

        return redirect('/admin/login')->with('error','Invalid login credentials');
    }
    */

    /*
    public function authJoin(Request $request){
        if ($request->admin == 0){
            if (Auth::guard('web')->attempt(['user_id' => $request->user_id, 'password' => $request->password])) {
                return redirect()->intended('/student');
            } else {
                return redirect('/login')->with('error','Invalid login credentials');
            }
        } elseif ($request->admin == 1){
            if (Auth::guard('admin')->attempt(['user_id' => $request->user_id, 'password' => $request->password])){
                return redirect()->intended('/admin');
            } else {
                return redirect('/admin/login')->with('error','Invalid login credentials');
            }
        }
    }
    */

    /*
    public function registerViewStudent(){
        return view('/admin/student_register');
    }

    public function registerSubmitStudent(Request $request){
        $validatedData = $request->validate([
            'user_id' => 'required|unique:students|max:255',
            // 'password' => 'required|min:6',
            'name' => 'required',
        ]);

        $student = new Student();
        $student->nim = $request->nim;
        $student->name = $request->name;
        $student->password = Hash::make(str_random(6));
        $student->save();

        return redirect('/login')->with('success', 'Registration process successful, please sign in');
    }
    */
    
    /*
    public function editAccount(Request $request){
        $name = $request->$name;
        $user_id = $request->$user_id;
        $password = $request->$password;

        $validatedData = $request->validate([
            'user_id' => 'required|unique:student|max:255',
            'password' => 'required|min:6',
            'name' => 'required',
        ]);

        $student = get(Writer::all());
        $student->name = $request->name;
        $student->user_id = $request->user_id;
        $student->password = Hash::make($request->password);
        $student->save();

        return redirect('/login')->with('success', 'Update Account successful, please sign in');
    }
    */

    public function registerViewAdmin(){
        return view('/admin_register');
    }

    public function registerSubmitAdmin(Request $request){
        $validatedData = $request->validate([
            'nim' => 'required|unique:admins|min:14|max:14',
            'password' => 'required|min:6',
            'name' => 'required',
        ]);

        $admin = new Admin();
        $admin->nim = $request->nim;
        $admin->name = $request->name;
        $admin->password = Hash::make($request->password);
        $admin->save();

        return redirect('/login')->with('success', 'Admin Registration process successful, please sign in');
    }

    public function logout(){
        Auth::guard('web')->logout();
        Auth::guard('admin')->logout();

        return redirect('/');
    }

    /*
    public function logout(){
        if (Auth::guard('web')->check()) {
            Auth::guard('web')->logout();
            return redirect('/login');
        } elseif(Auth::guard('admin')->check()) {
            Auth::guard('admin')->logout();
            return redirect('/admin/login');
        }
    }

    public function logoutAdmin(){
        return redirect('/logout');
    }
    */
}
